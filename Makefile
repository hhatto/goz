build: controller qiita hatebu stackoverflow reddit

controller: controller.go model.go
	go build -o bin/controller controller.go model.go const.go red.go

qiita: qiita.go model.go
	go build -o bin/qiita qiita.go model.go const.go red.go

hatebu: hatebu.go model.go
	go build -o bin/hatebu hatebu.go model.go const.go red.go

stackoverflow: stackoverflow.go model.go
	go build -o bin/stackoverflow stackoverflow.go model.go const.go red.go

reddit: reddit.go model.go
	go build -o bin/reddit reddit.go model.go const.go red.go

update-article:
	./bin/qiita
	./bin/stackoverflow
	./bin/reddit
	./bin/hatebu

install-requirements:
	cat requirements.txt | xargs go get -v

update-requirements:
	cat requirements.txt | xargs go get -u -v

test:
	go test ./filter

clean:
	rm ./bin/*
