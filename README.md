# goz

## about
(personal) go's news site.
powered by Go.

## Get Start
```
$ make build
$ make update-article
$ ./bin/controller
```

## Requirements
```
$ cat requirements.txt| xargs go get -u -v
```
and [forego](https://github.com/ddollar/forego)
```
$ brew install forego
```

## Original Source
* Qiita
* Hatebu
* stackoverflow
* Reddit
