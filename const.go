package main

const (
	ViaHatebu        = "b.hatena.ne.jp"
	ViaStackoverflow = "stackoverflow.com"
	ViaQiita         = "qiita.com"
	ViaReddit        = "reddit.com"
)
