package main

import (
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/tebeka/strftime"
	"github.com/gin-gonic/contrib/gzip"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/feeds"
)

var db = InitDB()
var favicon []byte

func getIndex(c *gin.Context) {
	var outputArticle []Article
	now := time.Now()
	targetTime := now.AddDate(0, 0, -7)
	start, _ := strftime.Format("%Y-%m-%d", targetTime)
	end, _ := strftime.Format("%Y-%m-%d", now)

	outputArticle, _ = getContent(db, start, false)
	obj := gin.H{"title": "goz - Go's News", "article": outputArticle, "start": start, "end": end}
	c.HTML(200, "index.html", obj)
}

func getDailyNews(c *gin.Context) {
	var outputArticle []Article
	date := c.Params.ByName("date")
	outputArticle, err := getContent(db, date, true)
	if err != nil {
		c.String(404, "page not found")
		return
	}

	t, err := time.Parse("2006-01-02", date)
	if err != nil {
		log.Printf("time.Parse error: %v", err)
		c.String(404, "page not found")
		return
	}

	prev, _ := strftime.Format("%Y-%m-%d", t.AddDate(0, 0, -1))

	n := t.AddDate(0, 0, 1)
	next, _ := strftime.Format("%Y-%m-%d", n)
	diff := time.Now().Sub(n).Seconds()
	if 0 >= diff {
		next = ""
	}

	obj := gin.H{"title": fmt.Sprintf("goz - Go's news %s", date),
		"article": outputArticle, "start": date, "end": date,
		"prevDay": prev, "nextDay": next, "daily": true}
	c.HTML(200, "index.html", obj)
}

func getAtom(c *gin.Context) {
	feed := getContents4Feed(db)
	atomData, _ := feed.ToAtom()

	c.Data(200, "application/atom+xml", []byte(atomData))
}

func getRSS(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "application/rss+xml")

	feed := getContents4Feed(db)
	//rssData, _ := feed.ToRss()
	_t := feeds.Rss{Feed: feed}
	rssFeed := _t.RssFeed()
	for r := range rssFeed.Items {
		rssFeed.Items[r].Enclosure = &feeds.RssEnclosure{}
	}
	rssData, _ := feeds.ToXML(rssFeed)

	c.Data(200, "application/rss+xml", []byte(rssData))
}

func getFavicon(c *gin.Context) {
	c.Data(200, "image/png", favicon)
}

func postPicks(c *gin.Context) {
	v, ok := c.GetPostForm("url")
	if !ok {
		log.Printf("not found data\n")
	}
	incPicks(v)
	//log.Printf(v)
}

func main() {
	var hostStr, chdir, mode string
	var err error
	flag.StringVar(&hostStr, "host", "localhost:8080", "hostname")
	flag.StringVar(&chdir, "workdir", ".", "change working directory")
	flag.StringVar(&mode, "mode", gin.ReleaseMode, "set mode (release, debug)")
	flag.Parse()

	ROOT, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	favicon, err = ioutil.ReadFile(filepath.Join(ROOT, "../static/favicon.ico"))
	if err != nil {
		log.Printf("favicon read error: %v\n", err)
		return
	}

	gin.SetMode(mode)
	os.Chdir(chdir)

	if mode == gin.DebugMode {
		db.LogMode(true)
	}

	r := gin.Default()
	r.Use(gzip.Gzip(gzip.DefaultCompression))
	r.Use(gin.Recovery())
	r.Use(gin.Logger())
	//r.LoadHTMLGlob("templates/*.html")
	funcMap := template.FuncMap{
		"baseurl": func(uri string) string {
			u, _ := url.Parse(uri)
			return u.Host
		},
	}
	html := template.Must(template.New("").Funcs(funcMap).ParseFiles("templates/index.html"))
	r.SetHTMLTemplate(html)

	r.GET("/", getIndex)
	r.GET("/daily/:date", getDailyNews)
	r.GET("/feed", getAtom)
	r.GET("/index.rss", getRSS)
	r.GET("/favicon.ico", getFavicon)
	r.POST("/picks", postPicks)
	r.Static("/static", filepath.Join(ROOT, "../static"))

	r.Run(hostStr)
}
