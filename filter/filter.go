package filter

import "strings"

func FilterIgnoreTitle(title string) bool {
	blackList := []string{
		"pokémon", "pokemon", "ポケモン",
		"囲碁", "碁",
	}

	t := strings.ToLower(title)
	for _, word := range blackList {
		if strings.Contains(t, word) {
			return true
		}
	}

	return false
}
