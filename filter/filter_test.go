package filter

import "testing"

func TestFilterIgnoreTitle(t *testing.T) {
	title := "Pokémon Goは、多くのことを教えてくれた | TechCrunch Japan"
	if !FilterIgnoreTitle(title) {
		t.Errorf("invalid. title=[%v]", title)
	}

	title = "Pokemon GO が、米国の夕方の公園の風景を一変させていた(松村太郎) - 個人 - Yahoo!ニュース"
	if !FilterIgnoreTitle(title) {
		t.Errorf("invalid. title=[%v]", title)
	}

	title = "『ポケモンGO』はもはや社会現象である！　 開発元Nianticのお膝元サンフランシスコでのリアルな『ポケモンGO』事情をレポート"
	if !FilterIgnoreTitle(title) {
		t.Errorf("invalid. title=[%v]", title)
	}
}
