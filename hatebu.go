package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"time"

	filter "./filter"

	"github.com/PuerkitoBio/goquery"
)

func main() {
	var doc *goquery.Document
	var err error
	db := InitDB()
	baseUrl := "http://b.hatena.ne.jp/search/tag?q=go&users=10"

	if doc, err = goquery.NewDocument(baseUrl); err != nil {
		log.Println("GET error")
		return
	}

	doc.Find("ul.search-result-list").Each(func(_ int, s *goquery.Selection) {
		titles := s.Find("h3 a").Map(func(_ int, s *goquery.Selection) string {
			title, _ := s.Attr("title")
			return title
		})
		hrefs := s.Find("h3 a").Map(func(_ int, s *goquery.Selection) string {
			link, _ := s.Attr("href")
			return link
		})
		users := s.Find("span.users").Map(func(_ int, s *goquery.Selection) string {
			t := s.Find("a span")
			return t.Text()
		})
		descs := s.Find(".entryinfo blockquote").Map(func(_ int, s *goquery.Selection) string {
			return s.Text()
		})
		dates := s.Find(".entryinfo blockquote span.created").Map(func(_ int, s *goquery.Selection) string {
			return s.Text()
		})
		var tags []bool
		s.Find(".entryinfo").Each(func(_ int, s *goquery.Selection) {
			foundGotag := false
			s.Find("a.tag").Each(func(__ int, s *goquery.Selection) {
				for _, v := range strings.Split(s.Text(), " ") {
					if strings.ToLower(v) == "go" || strings.ToLower(v) == "golang" {
						foundGotag = true
						return
					}
				}
			})
			tags = append(tags, foundGotag)
		})

		for i := range titles {
			if filter.FilterIgnoreTitle(titles[i]) {
				fmt.Println("skip:", titles[i])
				continue
			}

			now := time.Now()
			var userNum int
			if userNum, err = strconv.Atoi(users[i]); err != nil {
				log.Println(err)
				return
			}
			if userNum > 100 || !tags[i] {
				// 変な記事が混ざってる場合があるので除外する
				fmt.Println("skip:", titles[i])
				continue
			}

			h := md5.New()
			hashId := fmt.Sprintf("%s_%s_%s", ViaHatebu, titles[i], hrefs[i])
			io.WriteString(h, hashId)
			hashId = fmt.Sprintf("%x", h.Sum(nil))

			var exists []Article
			db.Where("hash_id = ?", hashId).First(&exists)
			if len(exists) != 0 {
				fmt.Println("already exists", exists[0].HashId)
				continue
			}

			tt := strings.Split(strings.Join(strings.Split(dates[i], " "), ""), "/")
			var year, month, day int
			if year, err = strconv.Atoi(tt[0]); err != nil {
				log.Println(err)
			}
			if month, err = strconv.Atoi(tt[1]); err != nil {
				log.Println(err)
			}
			if day, err = strconv.Atoi(tt[2]); err != nil {
				log.Println(err)
			}
			jst := time.FixedZone("JST", 9*60*60)
			t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, jst)
			article := Article{
				HashId:      hashId,
				Url:         hrefs[i],
				Title:       titles[i],
				Via:         ViaHatebu,
				Content:     strings.Join(strings.Split(descs[i], "\n")[2:], "\n"),
				IsPublished: false,
				Created:     now,
				Published:   t,
			}
			fmt.Println(article.Title)
			db.Save(&article)
		}
	})

}
