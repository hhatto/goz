package main

import (
	"crypto/md5"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"net/http"
)

// RssItem is content for index.rss
type RssItem struct {
	XMLName xml.Name `xml:"item,about"`
	Title   string   `xml:"title"` // required
	Link    string   `xml:"link"`  // required
	//Description string   `xml:"description"` // required
	//PubDate     string   `xml:"date"`        // created or updated
}

// Seq is RSS items
type Seq struct {
	List string `xml:"li"`
}

// RssChannel is description of RSS contents
type RssChannel struct {
	XMLName     xml.Name `xml:"channel"`
	About       string   `xml:"about,attr"`
	Title       string   `xml:"title"`
	Link        string   `xml:"link"`
	Description string   `xml:"description"`
	Items       []*Seq   `xml:"items>Seq"`
}

// RssItems is wrap of RssItem
type RssItems struct {
	XMLName xml.Name `xml:"RDF"`
	Channel *RssChannel
	Item    []*RssItem
}

func main() {
	db := InitDB()
	baseUrl := "http://b.hatena.ne.jp/search/tag?q=go&mode=rss&users=10"

	res, err := http.Get(baseUrl)
	if err != nil {
		log.Println("GET error")
		return
	}
	defer res.Body.Close()

	var items RssItems
	err = xml.NewDecoder(io.Reader(res.Body)).Decode(&items)
	if err != nil {
		log.Printf("rss decode error: %v", err)
		return
	}
	fmt.Println(items.Channel)

	var article Article
	for _, val := range items.Channel.Items {
		fmt.Println(val)
		h := md5.New()
		hashId := fmt.Sprintf("%s_%d", ViaHatebu, 1) //val.Link)
		io.WriteString(h, hashId)
		hashId = fmt.Sprintf("%x", h.Sum(nil))

		var exists []Article
		db.Where("hash_id = ?", hashId).First(&exists)
		if len(exists) != 0 {
			fmt.Println("already exists", exists[0].HashId)
			continue
		}

		//fmt.Println(val.PubDate, val.Title)
		//t := time.Unix(items[i].PubDate, 0)
		article = Article{
			HashId:      hashId,
			Via:         ViaHatebu,
			IsPublished: false,
		}
		fmt.Println(article)
		//db.Save(&article)
	}
}
