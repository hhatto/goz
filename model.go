package main

import (
	"bytes"
	"errors"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/tebeka/strftime"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/feeds"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

// Article is goz Content
type Article struct {
	Id          int64 `sql:"primary_key:yes;"`
	HashId      string
	Url         string
	Title       string
	Via         string
	Content     string `sql:"type:text;"`
	Description string
	Picks       int
	IsPublished bool
	Published   time.Time
	Created     time.Time
}

func getContent(db gorm.DB, date string, isDaily bool) (content []Article, err error) {
	var article []Article
	dateList := strings.SplitN(date, "-", 3)
	if len(dateList) != 3 {
		return nil, errors.New("invalid date string")
	}

	startTime := fmt.Sprintf("%s-%s-%s 00:00:00", dateList[0], dateList[1], dateList[2])
	if isDaily {
		endTime := fmt.Sprintf("%s-%s-%s 23:59:59", dateList[0], dateList[1], dateList[2])
		db.Order("created desc").Order("id desc").Where(
			"created <= ? AND created >= ?", endTime, startTime).Find(&article)
	} else {
		db.Order("created desc").Order("id desc").Where(
			"created >= ?", startTime).Find(&article)
	}

	notHatebuQuery := []string{ViaStackoverflow, ViaQiita, ViaReddit}
	var notHatenaArticle []Article
	db.Where("created >= ? AND via in (?)", startTime, notHatebuQuery).Find(&notHatenaArticle)

	for i, val := range article {
		isDuplicated := false
		if val.Via == "b.hatena.ne.jp" {
			for _, v := range notHatenaArticle {
				if v.Url == val.Url {
					isDuplicated = true
					break
				}
			}
			if isDuplicated {
				// duplicated article
				continue
			}
		}
		lineNum := len(strings.Split(val.Content, "\n"))
		if lineNum >= 3 {
			lineNum = 3
		}
		article[i].Content = strings.Join(strings.Split(val.Content, "\n")[:lineNum], "\n")
		val.Content = strings.Join(strings.Split(val.Content, "\n")[:lineNum], "\n")
		val.Picks, _ = getPicks(val.Url)
		content = append(content, val)
	}

	return content, nil
}

func getContents4Feed(db gorm.DB) (feed *feeds.Feed) {
	now := time.Now()
	rss := &feeds.Feed{
		Title:       "goz - Go's News",
		Link:        &feeds.Link{Href: "http://goz.hexacosa.net"},
		Description: "Go's curations site",
		Author:      &feeds.Author{Name: "Hideo Hattori", Email: "hhatto.jp@gmail.com"},
		Created:     now,
	}

	rss.Items = []*feeds.Item{}
	for i := 0; i < 8; i++ {
		if i == 0 && now.Hour() >= 0 && now.Hour() <= 9 {
			continue
		}
		var article []Article
		targetDate := now.AddDate(0, 0, (i+1)*(-1))

		strtime, _ := strftime.Format("%Y-%m-%d%%", targetDate)
		db.Where("created LIKE ?", strtime).Find(&article)
		//db.Where("created::text LIKE ?", strtime).Find(&article) // for PostgreSQL

		if len(article) == 0 {
			continue
		}

		desc := bytes.NewBufferString("<ul>")
		for _, val := range article {
			if val.Via == "b.hatena.ne.jp" {
				var checkArticle []Article
				db.Order("created desc").Order("id desc").Where(
					"created LIKE ? AND via != ? AND url = ?", strtime, "b.hatena.ne.jp", val.Url).Find(&checkArticle)
				//"created::text LIKE ? AND via != ? AND url = ?", strtime, "b.hatena.ne.jp", val.Url).Find(&checkArticle) // for PostgreSQL
				if len(checkArticle) >= 1 {
					// duplicated article
					continue
				}
			}
			desc.WriteString(fmt.Sprintf("<li><a href=\"%s\">%s</a></li>", val.Url, val.Title))
		}
		desc.WriteString("</ul>")

		jst := time.FixedZone("JST", 0*60*60)
		dateString := fmt.Sprintf("%d-%02d-%02d", targetDate.Year(), targetDate.Month(), targetDate.Day())
		feedLink := fmt.Sprintf("http://goz.hexacosa.net/daily/%s", dateString)
		rss.Items = append(rss.Items, &feeds.Item{
			Title:       fmt.Sprintf("goz - %s", dateString),
			Link:        &feeds.Link{Href: feedLink},
			Description: desc.String(),
			Author:      &feeds.Author{Name: "goz team", Email: ""},
			Created:     time.Date(targetDate.Year(), targetDate.Month(), targetDate.Day(), 0, 0, 0, 0, jst),
		})
		desc.Reset()
	}

	return rss
}

// InitDB is initialize Database
func InitDB() gorm.DB {
	//db, err := gorm.Open("postgres", "user=goz dbname=goz sslmode=disable")
	//db, err := gorm.Open("mysql", "goz:goz@/goz?charset=utf8&parseTime=True")
	db, err := gorm.Open("sqlite3", "goz.sqlite3")
	//db, err := gorm.Open("sqlite3", ":memory:")
	if err != nil {
		panic(fmt.Sprintf("db.open() error: %v", err))
	}

	if !db.HasTable(&Article{}) {
		db.CreateTable(Article{})
		db.Model(&Article{}).AddIndex("idx_article_created", "created")
		db.Model(&Article{}).AddIndex("idx_article_via", "via")
	}

	return db
}
