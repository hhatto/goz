package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

// QiitaItem is Qiita Content
type QiitaItem []struct {
	Id           int64  `json:"id"`
	Title        string `json:"title"`
	Url          string `json:"url"`
	StockCount   int64  `json:"stock_count"`
	CommentCount int64  `json:"comment_count"`
	Content      string `json:"raw_body"`
	Created      int64  `json:"created_at_as_seconds"`
}

func main() {
	db := InitDB()
	baseUrl := "https://qiita.com/api/v1/tags/Go/items?per_page=100"

	res, err := http.Get(baseUrl)
	if err != nil {
		log.Printf("GET error: %v", err)
		return
	}
	defer res.Body.Close()

	var items QiitaItem
	err = json.NewDecoder(io.Reader(res.Body)).Decode(&items)
	if err != nil {
		log.Printf("qiita. json decode error: %v", err)
		return
	}

	var article Article
	for i := range items {
		now := time.Now()
		h := md5.New()
		hashId := fmt.Sprintf("%s_%d", ViaQiita, items[i].Id)
		io.WriteString(h, hashId)
		hashId = fmt.Sprintf("%x", h.Sum(nil))

		var exists []Article
		db.Where("hash_id = ?", hashId).First(&exists)
		if len(exists) != 0 {
			fmt.Println("already exists", exists[0].HashId)
			continue
		}

		if items[i].StockCount >= 10 || items[i].CommentCount > 2 {
			fmt.Println(items[i].Created, items[i].Title)
			t := time.Unix(items[i].Created, 0)
			article = Article{
				HashId:      hashId,
				Url:         items[i].Url,
				Title:       items[i].Title,
				Via:         ViaQiita,
				Content:     items[i].Content,
				IsPublished: false,
				Published:   t,
				Created:     now,
			}
			db.Save(&article)
		} else {
			fmt.Println(items[i].Title, items[i].Url)
		}
	}
}
