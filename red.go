package main

import (
	"log"

	"github.com/garyburd/redigo/redis"
)

const PICKS_KEY_PREFIX = "goz_picks_"
const INC_PICKS_SCRIPT = `local current
current = redis.call("incr",KEYS[1])
if tonumber(current) == 1 then
  redis.call("expire",KEYS[1],3600)
end
` // x/1[click/hour]

func incPicks(url string) {
	c, err := redis.Dial("tcp", ":6379")
	if err != nil {
		log.Printf("redis conn error. err=[%v]", err)
		return
	}

	key := PICKS_KEY_PREFIX + url
	script := redis.NewScript(1, INC_PICKS_SCRIPT)
	_, err = script.Do(c, key)
	return
}

func getPicks(url string) (num int, err error) {
	c, err := redis.Dial("tcp", ":6379")
	if err != nil {
		log.Printf("redis conn error. err=[%v]", err)
		return
	}

	key := PICKS_KEY_PREFIX + url
	num, err = redis.Int(c.Do("GET", key))
	if err != nil {
		//log.Printf("redis.GET(key=%v) error. err=[%v]", key, err)
		return
	}

	return
}
