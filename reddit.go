package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

// RedditData is description of reddit.com item
type RedditData struct {
	Id           string  `json:"id"`
	Title        string  `json:"title"`
	Url          string  `json:"url"`
	Score        int64   `json:"score"`
	CommentCount int64   `json:"num_comments"`
	Domain       string  `json:"domain"`
	Created      float64 `json:"created_utc"`
}

// RedditItem is Item for reddit.com content
type RedditItem struct {
	Kind string     `json:"kind"`
	Item RedditData `json:"data"`
}

// RedditItems is reddit.com item wrapper
type RedditItems struct {
	modhash string       `json:"modhash"`
	Items   []RedditItem `json:"children"`
}

// Reddit is reddit.com resource
type Reddit struct {
	Kind string      `json:"kind"`
	Item RedditItems `json:"data"`
}

const (
	SELF_SCORE    = 20
	SELF_COMMENTS = 30
	SCORE         = 20
	COMMENTS      = 10
	SELF_DOMAIN   = "self.golang"
)

func main() {
	db := InitDB()
	baseUrl := "http://www.reddit.com/r/golang/hot/.json?limit=100&t=week"

	res, err := http.Get(baseUrl)
	if err != nil {
		log.Printf("GET error: %v", err)
		return
	}
	defer res.Body.Close()

	var items Reddit
	err = json.NewDecoder(io.Reader(res.Body)).Decode(&items)
	if err != nil {
		log.Printf("reddit. json decode error: %v", err)
		return
	}

	var article Article
	for _, v := range items.Item.Items {
		now := time.Now()
		val := v.Item
		h := md5.New()
		hashId := fmt.Sprintf("%s_%s_%s", ViaReddit, val.Id, val.Title)
		io.WriteString(h, hashId)
		hashId = fmt.Sprintf("%x", h.Sum(nil))

		var exists []Article
		db.Where("hash_id = ?", hashId).First(&exists)
		if len(exists) != 0 {
			fmt.Println("already exists", exists[0].HashId)
			continue
		}

		if (val.Domain == SELF_DOMAIN && val.Score >= SELF_SCORE) ||
			(val.Domain == SELF_DOMAIN && val.CommentCount >= SELF_COMMENTS) ||
			(val.Domain != SELF_DOMAIN && val.Score >= SCORE) ||
			(val.Domain != SELF_DOMAIN && val.CommentCount >= COMMENTS) {
			t := time.Unix(int64(val.Created), 0)
			article = Article{
				HashId:      hashId,
				Url:         val.Url,
				Title:       val.Title,
				Via:         ViaReddit,
				Content:     "",
				IsPublished: false,
				Created:     now,
				Published:   t,
			}
			db.Save(&article)
		} else {
			fmt.Println(val.Title, val.Url)
		}
	}
}
