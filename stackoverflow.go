package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

// StackoverflowItem is stackoverflow.com content
type StackoverflowItem struct {
	Id          int64  `json:"question_id"`
	Title       string `json:"title"`
	Url         string `json:"link"`
	Score       int64  `json:"score"`
	AnswerCount int64  `json:"answer_count"`
	Created     int64  `json:"last_activity_date"`
}

// StackoverflowItems is items for stackoverflow.com
type StackoverflowItems struct {
	Items []StackoverflowItem `json:"items"`
}

func main() {
	db := InitDB()
	baseUrl := "https://api.stackexchange.com/2.1/questions/featured?site=stackoverflow&tagged=go&pagesize=100&sort=votes&answers=1&min=1"

	res, err := http.Get(baseUrl)
	if err != nil {
		log.Println("GET error")
		return
	}
	defer res.Body.Close()

	var items StackoverflowItems
	err = json.NewDecoder(io.Reader(res.Body)).Decode(&items)
	if err != nil {
		fmt.Println(err)
		log.Println("json decode error")
	}

	var article Article
	for _, val := range items.Items {
		now := time.Now()
		h := md5.New()
		hashId := fmt.Sprintf("%s_%d_%s", ViaStackoverflow, val.Id, val.Title)
		io.WriteString(h, hashId)
		hashId = fmt.Sprintf("%x", h.Sum(nil))

		var exists []Article
		db.Where("hash_id = ?", hashId).First(&exists)
		if len(exists) != 0 {
			fmt.Println("already exists", exists[0].HashId)
			continue
		}

		fmt.Println(val)
		if val.Score >= 1 || val.AnswerCount >= 1 {
			t := time.Unix(val.Created, 0)
			article = Article{
				HashId:      hashId,
				Url:         val.Url,
				Title:       val.Title,
				Via:         ViaStackoverflow,
				Content:     "",
				IsPublished: false,
				Created:     now,
				Published:   t,
			}
			db.Save(&article)
		} else {
			fmt.Println(val.Title, val.Url)
		}
	}
}
